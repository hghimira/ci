Module                  Size  Used by
nfnetlink              32768  0
br_netfilter           49152  0
overlay               208896  0
x86_pkg_temp_thermal    16384  0
coretemp               16384  0
kvm_intel             311296  0
kvm                  1363968  1 kvm_intel
irqbypass              12288  1 kvm
wmi_bmof               12288  0
mei_pxp                16384  0
mei_hdcp               28672  0
intel_pmc_core        114688  0
intel_vsec             20480  1 intel_pmc_core
pmt_telemetry          16384  1 intel_pmc_core
pmt_class              12288  1 pmt_telemetry
fuse                  188416  5
drm                   757760  0
drm_panel_orientation_quirks    24576  1 drm
ip_tables              24576  0
x_tables               57344  1 ip_tables
e1000e                352256  0
crct10dif_pclmul       12288  1
crc32_pclmul           12288  0
ptp                    45056  1 e1000e
ghash_clmulni_intel    16384  0
i2c_i801               40960  0
i2c_smbus              20480  1 i2c_i801
pps_core               32768  1 ptp
video                  77824  0
wmi                    28672  2 video,wmi_bmof
